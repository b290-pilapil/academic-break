function shipItem(item, weight, location){
    // guard clause technique XD
    if(item==''|| item==undefined || weight==''|| weight==undefined || location==''|| location==undefined)
        return "Inputs must not be empty"

    let totalShippingFee;
    if(weight<=3)
        totalShippingFee=150
    else if(weight<=5)
        totalShippingFee=280
    else if(weight<=8)
        totalShippingFee=340
    else if(weight<=10)
        totalShippingFee=410
    else   
        totalShippingFee=560
    switch(location){
        case "local":
            break
        case "international":
            totalShippingFee+=250
            break
        default:
            break
    }
    // grammar changes + extra string template to consider a/an (nao-OC ako)
    return `The total shipping fee for a${["a", "e", "i", "o", "u"].includes(item[0].toLowerCase())?'n':''} ${item} that will be shipped ${location}ly is PHP${totalShippingFee}`
}
console.log(shipItem("crate of Ryzen 5 3600 CPUs", 5, "local"))

function Product(name, price, quantity){
    this.name = name
    this.price = price
    this.quantity = quantity
}

function Customer(name){
    this.name = name
    this.cart = []
    this.addToCart = function(product){
        this.cart.push(product)
        return `${product.name} is added to cart.`
    }
    // annoying na hindi to removeFromCart
    this.removeToCart = function(product){
        if(this.cart.includes(product)){
            this.cart.splice(this.cart.indexOf(product), 1)
            return `${product.name} is removed from your cart`
            // optional code, meant to provide more context
            // but may be unecessary considering end users will most likely interact via GUI
        // }else{
        //     return `${product.name} is not in your cart`
        }
    }
}
let cust = new Customer("Clyde")
let prod = new Product("Ryzen 5 3600 CPU", 8000, 100)

console.log(cust.removeToCart(prod)) //returns nothing/undefined
console.log(cust.addToCart(prod))
console.log(cust)
console.log(cust.removeToCart(prod))
console.log(cust)